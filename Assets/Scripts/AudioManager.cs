﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource audioSource;
    public float volume = 1.0f;

    public bool mute = false; //True means muted


    void Start()
    {
        if (mute)
        {
            volume = 0.0f;
        }
        audioSource.volume = volume;
        audioSource.Play();
    }

   

   

}
