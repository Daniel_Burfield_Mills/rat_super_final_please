﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class smoothcamerafollow : MonoBehaviour
{
    public Transform target;
    public Vector3 offset;
    [Range (1,10)]
    public float smoothness;

    void Update()
    {
        SmoothFollow();
    }

    void SmoothFollow()
    {
        Vector3 targetPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, targetPosition, smoothness * Time.deltaTime);
        transform.position = targetPosition;
    }
}
