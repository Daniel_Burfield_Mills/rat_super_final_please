﻿using System.Collections;
using System.Collections.Generic;
using Boo.Lang.Runtime.DynamicDispatching;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour
{
    public Rigidbody2D playerRigidbody;
    public float moveSpeed = 1f;
    public float maxSpeed = 100f;
    public Animator animator;
    public float runSpeedThreshold;
    public bool grounded;
    public LayerMask ground;

    public void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
    }


    void Update()
    {
        if (Mathf.Abs(playerRigidbody.velocity.x) >= maxSpeed)
        {
            playerRigidbody.velocity = new Vector2((playerRigidbody.velocity.x < 0)? -maxSpeed : maxSpeed, 0);
        }

        if (playerRigidbody != null)
        {
            ApplyInput();

        }
        else
        {
            Debug.LogWarning("Rigid body not attached to player" + gameObject.name);
        }


        //animation controlling. Dont fuck any of it up
        //if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > runSpeedThreshold)
        if ((GetComponent<Rigidbody2D>().velocity.x > runSpeedThreshold))
        {
            animator.SetFloat("Speed", 1); // running in here
        }
        else
        {
           animator.SetFloat("Speed", 0); // not running in here
        }

        RaycastHit2D groundedHit = Physics2D.CircleCast(transform.position, GetComponent<CircleCollider2D>().radius, -Vector2.up, GetComponent<CircleCollider2D>().radius + 0.01f, ground);
        grounded = groundedHit ? true : false;

        if(grounded)
        {
            animator.SetInteger("Y", 0);
        }
        else if ((GetComponent<Rigidbody2D>().velocity.y > 0))
        {
            animator.SetInteger("Y", 1);
        }
        else
        {
            animator.SetInteger("Y", -1);
        }



        if ((GetComponent<Rigidbody2D>().velocity.y > 1))
        {
            animator.SetInteger("Y", 1);
        }
        else
        {
            animator.SetInteger("Y", -1);
        }

        Vector3 characterScale = transform.localScale;
        if(Input.GetKeyDown(KeyCode.D))
        {
            characterScale.x = 1;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            characterScale.x = -1;
        }
        transform.localScale = characterScale;

        


        //animation stuff ends here

        
    }

    public void ApplyInput()
    {
        float xInput = Input.GetAxis("Horizontal");
        float yInput = Input.GetAxis("Vertical");
        Vector2 multi = new Vector2(1,0);

        if (Mathf.Abs(playerRigidbody.velocity.x) < 1)
        {
           // multi = new Vector2(100, 0);
        }
      

        float xForce = xInput * moveSpeed * Time.deltaTime;

        Vector2 force = new Vector2(xForce, 0);

        playerRigidbody.AddForce(force * multi);
    }
}
