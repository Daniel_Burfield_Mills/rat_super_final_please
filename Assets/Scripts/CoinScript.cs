﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour
{
    public AudioClip coinPick;
    public float volume = 1.0f;

    void OnTriggerEnter2D(Collider2D col)
    {
        PlayerPrefs.SetInt("Coins", ScoreTextScript.coinAmount += 1);
        AudioSource.PlayClipAtPoint(coinPick, transform.position, volume);
        Destroy(gameObject);
    }
}
