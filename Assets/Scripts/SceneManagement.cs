﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class sceneManagement : MonoBehaviour
{

    public string Menu;
    public string Shop;
    public string LevelSelect;
    public string Level1;
    public string Level2;
    public string Level3;


    



    public void SceneMenu()
    {
        SceneManager.LoadScene(Menu);
    }

    public void SceneShop()
    {
        SceneManager.LoadScene(Shop);
    }

    public void SceneLevelSelect()
    {
        SceneManager.LoadScene(LevelSelect);
    }

    public void SceneLevel1()
    {
        SceneManager.LoadScene(Level1);
    }

    public void SceneLevel2()
    {
        SceneManager.LoadScene(Level2);
    }

    public void SceneLevel3()
    {
        SceneManager.LoadScene(Level3);
    }


    public void closeGame()
    {
        Debug.Log("Game quit");
        Application.Quit();
    }

    public void Reset()
    {
        PlayerPrefs.SetInt("Coins", 0);
    }
}
