﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class hatPlacement : MonoBehaviour
{
    public SpriteRenderer hat;
    public Sprite shrek;
    public Sprite santa;
    public Sprite cowboy;
    public Sprite irish;
    public Sprite tophat;
    public Sprite maga;
    public Sprite kid;
    public Sprite princess;
    public Sprite russian;

    public int cost = 25;

    bool shrekBuy = false, santaBuy = false, cowboyBuy = false, irishBuy = false, tophatBuy = false, magaBuy = false, kidBuy = false, princessBuy = false, russianBuy = false;


    private void Start()
    {
        updatePurchase();



    }
    private void updatePurchase()
    {

        PlayerPrefs.SetString("shrekBuy", shrekBuy.ToString());
        PlayerPrefs.SetString("santaBuy", santaBuy.ToString());
        PlayerPrefs.SetString("cowboyBuy", cowboyBuy.ToString());
        PlayerPrefs.SetString("irishBuy", irishBuy.ToString());
        PlayerPrefs.SetString("tophatBuy", tophatBuy.ToString());
        PlayerPrefs.SetString("magaBuy", magaBuy.ToString());
        PlayerPrefs.SetString("kidBuy", kidBuy.ToString());
        PlayerPrefs.SetString("princessBuy", princessBuy.ToString());
        PlayerPrefs.SetString("russianBuy", russianBuy.ToString());

        shrekBuy = Boolean.Parse(PlayerPrefs.GetString("shrekBuy", "false"));
        santaBuy = Boolean.Parse(PlayerPrefs.GetString("santaBuy", "false"));
        cowboyBuy = Boolean.Parse(PlayerPrefs.GetString("cowboyBuy", "false"));
        irishBuy = Boolean.Parse(PlayerPrefs.GetString("irishBuy", "false"));
        tophatBuy = Boolean.Parse(PlayerPrefs.GetString("tophatBuy", "false"));
        magaBuy = Boolean.Parse(PlayerPrefs.GetString("magaBuy", "false"));
        kidBuy = Boolean.Parse(PlayerPrefs.GetString("kidBuy", "false"));
        princessBuy = Boolean.Parse(PlayerPrefs.GetString("princessBuy", "false"));
        russianBuy = Boolean.Parse(PlayerPrefs.GetString("russianBuy", "false"));



    }

    public void Shrek()
    {

        if (shrekBuy == false)
        {
            if (PlayerPrefs.GetInt("Coins") < cost)
            {
                Debug.Log("You are too poor!");
            }
            else if (PlayerPrefs.GetInt("Coins") >= cost)
            {
                PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - cost);
                hat.sprite = shrek;
                shrekBuy = true;
            }
        }
        else if (shrekBuy == true)
        {
            hat.sprite = shrek;
        }

    }

    public void Santa()
    {

        if (santaBuy == false)
        {
            if (PlayerPrefs.GetInt("Coins") < cost)
            {
                Debug.Log("You are too poor!");
            }
            else if (PlayerPrefs.GetInt("Coins") >= cost)
            {
                PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - cost);
                hat.sprite = santa;
                santaBuy = true;
            }
        }
        else if (santaBuy == true)
        {
            hat.sprite = santa;
        }
    }

    public void Cowboy()
    {
        if (cowboyBuy == false)
        {
            if (PlayerPrefs.GetInt("Coins") < cost)
            {
                Debug.Log("You are too poor!");
            }
            else if (PlayerPrefs.GetInt("Coins") >= cost)
            {
                PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - cost);
                hat.sprite = cowboy;
                cowboyBuy = true;
            }
        }
        else if (cowboyBuy == true)
        {
            hat.sprite = cowboy;
        }
    }

    public void Irish()
    {
        if (irishBuy == false)
        {
            if (PlayerPrefs.GetInt("Coins") < cost)
            {
                Debug.Log("You are too poor!");
            }
            else if (PlayerPrefs.GetInt("Coins") >= cost)
            {
                PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - cost);
                hat.sprite = irish;
                irishBuy = true;
            }
        }
        else if (irishBuy == true)
        {
            hat.sprite = irish;
        }
    }

    public void Tophat()
    {
        if (tophatBuy == false)
        {
            if (PlayerPrefs.GetInt("Coins") < cost)
            {
                Debug.Log("You are too poor!");
            }
            else if (PlayerPrefs.GetInt("Coins") >= cost)
            {
                PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - cost);
                hat.sprite = tophat;
                tophatBuy = true;
            }
        }
        else if (tophatBuy == true)
        {
            hat.sprite = tophat;
        }
    }

    public void MAGA()
    {
        if (magaBuy == false)
        {
            if (PlayerPrefs.GetInt("Coins") < cost)
            {
                Debug.Log("You are too poor!");
            }
            else if (PlayerPrefs.GetInt("Coins") >= cost)
            {
                PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - cost);
                hat.sprite = maga;
                magaBuy = true;
            }
        }
        else if (magaBuy == true)
        {
            hat.sprite = maga;
        }
    }

    public void Kid()
    {
        if (kidBuy == false)
        {
            if (PlayerPrefs.GetInt("Coins") < cost)
            {
                Debug.Log("You are too poor!");
            }
            else if (PlayerPrefs.GetInt("Coins") >= cost)
            {
                PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - cost);
                hat.sprite = kid;
                kidBuy = true;
            }
        }
        else if (kidBuy == true)
        {
            hat.sprite = kid;
        }
    }

    public void Princess()
    {
        if (princessBuy == false)
        {
            if (PlayerPrefs.GetInt("Coins") < cost)
            {
                Debug.Log("You are too poor!");
            }
            else if (PlayerPrefs.GetInt("Coins") >= cost)
            {
                PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - cost);
                hat.sprite = princess;
                princessBuy = true;
            }
        }
        else if (princessBuy == true)
        {
            hat.sprite = princess;
        }
    }

    public void Russian()
    {
        if (russianBuy == false)
        {
            if (PlayerPrefs.GetInt("Coins") < cost)
            {
                Debug.Log("You are too poor!");
            }
            else if (PlayerPrefs.GetInt("Coins") >= cost)
            {
                PlayerPrefs.SetInt("Coins", PlayerPrefs.GetInt("Coins") - cost);
                hat.sprite = russian;
                russianBuy = true;
            }
        }
        else if (russianBuy == true)
        {
            hat.sprite = russian;
        }
    }
}
