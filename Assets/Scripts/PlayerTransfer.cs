﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerTransfer : MonoBehaviour
{
    public static PlayerTransfer instance;
    private static string mainMenuScene = "MainMenu";
    private string cSceneName;
    public Transform mainBody;
    private static Vector3 initialPos;

    void Awake()
    {
        initialPos = gameObject.transform.position;
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            Debug.Log("Player Was Not Destroyed");
        }
        else
        {
            Destroy(gameObject);
            Debug.Log("object was destroyed");
        }
    }

    void LateUpdate()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        cSceneName = currentScene.name;

        if (cSceneName == mainMenuScene)
        {
            GetComponentInChildren<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            mainBody.transform.position = initialPos;
            Debug.Log("the initial Pos was got" + initialPos);
        }
        else
        {
            GetComponentInChildren<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        }
    }
}
