﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTextScript : MonoBehaviour
{
    Text text;
    public static int coinAmount;

    void Start()
    {
        coinAmount = PlayerPrefs.GetInt("Coins", 0);
        text = GetComponent<Text>();
        text.text = coinAmount.ToString();
    }
    
    void Update()
    {
        text.text = coinAmount.ToString();
    }
}
