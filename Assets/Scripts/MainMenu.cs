﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : MonoBehaviour
{

    public string Scene;

    public void sceneTransition()
    {
        SceneManager.LoadScene(Scene);
    }

    public void closeGame()
    {
        Debug.Log("Game quit");
        Application.Quit();
    }
}
